# reveal-md

## install

npm install reveal-md

## usage

npx reveal-md ppt.md

## static sites

npx reveal-md ppt.md --static dist

## gitpages

https://wyl350.gitee.io/my-work-miner

source: https://gitee.com/wyl350/my-work-miner/tree/master/dist

