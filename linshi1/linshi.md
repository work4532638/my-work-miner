##

法律分析：最高人民法院只有一个,总部在北京。但最高法有6个巡回法庭，涵盖华东、华中、华南、西北、西南、华北。
第一巡回法庭设在广东省深圳市，巡回区为广东、广西、海南、湖南四省；
第二巡回法庭设在辽宁省沈阳市，巡回区为辽宁、吉林、黑龙江三省；
第三巡回法庭设在江苏省南京市，巡回区为江苏、上海、浙江、福建、江西五省市；
第四巡回法庭设在河南省郑州市，巡回区为河南、山西、湖北、安徽四省；
第五巡回法庭设在重庆市，巡回区为重庆、四川、贵州、云南、西藏五省区；
第六巡回法庭设在陕西省西安市，巡回区为陕西、甘肃、青海、宁夏、新疆五省区。

法律依据：《中华人民共和国人民法院组织法》 第十九条 最高人民法院可以设巡回法庭，审理最高人民法院依法确定的案件。

巡回法庭是最高人民法院的组成部分。巡回法庭的判决和裁定即最高人民法院的判决和裁定。

##

网络来源： https://m.thepaper.cn/baijiahao_23495448

2022年度律师、基层法律服务工作统计分析
政务：广东司法行政 2023-06-15 17:03
近日，司法部发布《2022年度律师、基层法律服务工作统计分析》。统计数据显示，截至2022年底，全国共有执业律师65.16万多人，律师事务所3.86万多家。全国基层法律服务机构1.3万多家，基层法律服务工作者5.6万多人。

对比2021年统计数据，2022年全国执业律师人数比上一年度新增7.68万，涨幅超13%。律师人数超过1万人的省（区、市）有23个；超过3万人的省（市）有8个，分别是广东、北京、江苏、上海、山东、浙江、四川、河南，河南首次进入“3万+”序列。全国律师事务所新增2100多家；律师100人以上的律师事务所500家，增幅超20%。

律师工作

1

（一）律师。截至2022年底，全国共有执业律师65.16万多人。律师人数超过1万人的省（区、市）有23个，其中超过3万人的省（市）有8个（分别是广东、北京、江苏、上海、山东、浙江、四川、河南）。

从律师类别看，专职律师50.47万多人，占77.46%，兼职律师1.43万多人，占2.19%，公职律师9.59万多人，占14.73%，公司律师2.99万多人，占4.6%，军队律师1500多人，占0.23%。

![2022年全国各类律师对比](imgs/1.jpg)

从年龄结构看，30岁以下的律师10.79万多人，占16.56%，30岁（含）至50岁的律师41.95万多人，占64.39%，50岁（含）至65岁的律师10.63万多人，占16.32%，65岁（含）以上的律师1.77万多人，占2.73%。

![2022年全国律师年龄结构占比](imgs/2.jpg)

从文化程度看，本科学历的律师46.69万多人，占71.66%，硕士研究生学历的律师13.04万多人，占20.01%，博士研究生学历的律师7800多人，占1.21%，本科学历以下的律师4.64万多人，占7.12%。

在境外接受过教育并获得学位的律师8727人，占1.34%。

![2022年全国律师教育程度占比](imgs/3.jpg)

（二）律师事务所。截至2022年底，全国共有律师事务所3.86万多家。其中，合伙所2.82万多家，占73.16%，国资所604家，占1.56%，个人所9777家，占25.28%。

![2022年全国律师事务所分类情况统计](imgs/4.jpg)

从律师事务所规模来看，律师10人（含）以下的律师事务所2.53万多家，占65.5%，律师11人至20人（含）的律师事务所8023家，占20.74%，律师21人至50人（含）的律师事务所4037家，占10.44%，律师51人至100人（含）的律师事务所784家，占2.03%，律师100人以上的律师事务所500家，占1.29%。

![2022年全国律师事务所规模统计](imgs/5.jpg)

（三）律师业务。2022年，全国律师办理各类法律事务1274.4万多件。其中，办理诉讼案件824.4万多件，办理非诉讼法律事务141.6万多件，为87.6万多家党政机关、人民团体和企事业单位等担任法律顾问。

在律师办理的824.4万多件诉讼案件中，刑事诉讼辩护及代理99万多件，占诉讼案件的12.01%；民事诉讼代理697.5万多件，占诉讼案件的84.61%；行政诉讼代理25.4万多件，占诉讼案件的3.09%；代理申诉2.3万多件，占诉讼案件的0.29%。


![2022年全国诉讼案件辩护及代理情况](imgs/6.jpg)

2022年，律师共提供各类公益法律服务141.3万多件，其中办理法律援助案件97.1万多件，参与接待和处理信访案件21.6万多件，律师调解20.9万多件，参与处置城管执法事件1.6万多件。律师为弱势群体提供免费法律服务130.9万多件。律师等法律从业人员为60万多个村（居）担任法律顾问。

（四）律师对外开放。截至2022年底，已有来自22个国家和地区的217家律师事务所在华（内地、大陆）设立282家代表机构，其中外国律师事务所驻华代表机构205家，香港律师事务所驻内地代表机构64家，台湾律师事务所驻大陆代表机构13家，港澳律师事务所与内地律师事务所建立了25家合伙型联营律师事务所，有8家在上海自贸区设立代表机构的外国律师事务所与中国律师事务所实行联营。我国律师事务所在境外设立分支机构共180家。

![](imgs/7.jpg)

（五）律师参政议政。截至2022年底，律师担任“两代表一委员”共12017人，其中担任各级人大代表4219人，担任各级政协委员7067人，担任各级党代会代表731人。

（六）律师惩戒。2022年，106家律师事务所受到行政处罚，309家律师事务所受到行业惩戒；540名律师受到行政处罚，1131名律师受到行业惩戒。

基层法律服务工作

2

（一）机构队伍。截至2022年底，全国共有基层法律服务机构1.3万多家，其中乡镇所7900多家，占57.47%；街道所5800多家，占42.53%。全国基层法律服务工作者5.6万多人，其中在乡镇所执业的基层法律服务工作者2.4万多人，占43.07%，在街道所执业的基层法律服务工作者3.2万多人，占56.93%。

（二）基层法律服务业务。2022年，全国基层法律服务工作者共办理诉讼案件63.4万多件；办理非诉讼法律事务13万多件；为6.3万多家党政机关、人民团体、企事业单位担任法律顾问；参与仲裁5万多件。

2022年，基层法律服务工作者共提供各类公益法律服务35.1万多件，其中办理法律援助案件12.2万多件，参与人民调解18.6万多件，参与接待和处理信访案件4.3万多件。基层法律服务工作者为12.6万多个村（居）担任法律顾问，为弱势群体提供免费法律服务45.1万多件。



###

我在前几天跟人事冯秋霞发过微信，到你们庭室，但是他没有回我，我现在不清楚她是什么意思，当然不管分到哪里对我来讲，都是不错的。

我在督察室工作，这么多年，自从离开立案庭，我几乎就没有看过或者思考过任何法律相关内容。这些年我都做什么了？我除了把我的女儿照顾的还差不多，就是学习编程。写各种小的项目。看技术文档，主要是各种前端技术的说明性文档。工作反而是我的副业，基本上能保证一两个月的工作量压起来，我一两天就料理完了。

我为什么选择，重新回到审判岗位？
1. 这些年工作有点碌碌无为
1. 尽管自己差不多算是一个老同志了，但是 被边缘的较为厉害，可能自己的心理承受不了吧。尽管我还不是特别功利，非要怎么怎么样
1. 我曾今的理想就是做一名法官，后来各种原因离开了，现在不忘初衷，又想找回理想
1. 当然也有一定的原因是，现在员额较难进入，我作为最后任命的助理审判员，能够入额的机会可能不多了

如果我入额了，我的得失？
1. 我将失去我在行政级别上的可能的升迁，还有就是三级主任科员
1. 重回审判岗位，可能会得到未来
如果我入额了，我该如何去做？
1. 

田兰，你办理案件还是非常认真的，看到你整理的东西，我还是大为震动。

你的想法很好，我其实在以前就做过一个关于立案庭如何办理不予受理、管辖类案件的法律适用的相关法条的整理（电子版，word格式保存）。尽管花了心思整理了，但是东西还是丢了。现在竟然找不见了。这些年我除了计算机相关的东西学了些，法律相关的东西很多都淡忘了。

重返审判岗位，我还是有一定计划的。

我现在有没有办案能力，以及如何快速适应未来办案要求？
1. 我不是谦虚，我目前的水平是无法办理任何案件的，而且我现在入额了，作为法官，拿不下案子是肯定不行的。
1. 

