##

[《工伤保险条例》](./工伤保险条例.md)

职工发生工伤后，可以申请工伤鉴定。

工伤鉴定可以是用人单位申请，也可以是劳动者自己申请。

《工伤认定办法》: 
工伤认定要准备材料，
1. 填写《工伤认定申请表》
1. 能够证明与用人单位存在劳动关系的材料，例如： 劳动合同
1. 医疗机构出局的受伤后诊断证明书 或者 职业病诊断证明书
1. 在实际中，有违法转包、分包的情形，这并不影响用工单位承担工伤保险责任。
将以上的材料交到社保部门，
社保行政部门应该在十五日内对材料进行审核，然后作出受理或者不受理的决定，并且出局相应的决定书。应当受理的工伤认定申请，要在六十日内作出送上认定决定。
2022年国家根据上一年的城镇居民人均可支配收入，对一次性工亡补助金标准做出了新的调整。因工伤致残，被鉴定为一到十级伤残的，一次性伤残补助金分别是本人工资乘以七到二十七个月不等。一级至六级伤残的，还可以按照个人工资比例按月享受伤残津贴。例如： 某人因受伤被鉴定为五级伤残。受伤前十二个月平均工资为四千，那工伤保险基金要一次性支付 四千乘以 十八个月。也就是 72000 。
如果用人单位难以安排工作，还要每月支付 四千乘以百分之七十工资。如果这个金额低于当地最低工资标准，用人单位要补足差额。
伤残等级的确定没有同意的标准，所以各个省份的情况有所不同，但是对于什么样的标准，对于工伤致残的人来说，这份赔偿金是一份安慰，也是一种帮助。

